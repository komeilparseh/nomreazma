# CONTRIBUTING

- 1. To contribute to this project, please [fork](https://github.com/Online-test-system-Nomre/Nomreazma/fork) the project 

- 2.  `git clone git@github.com:YOUR_GITHUB_USERNAME/Nomreazma.git`
- 3.  `cd Nomreazma`
- 4.  `git checkout -b yourfeature`
- 5. Work on the feature!
- 6. Finally, please run tests

# Pull Request Template

- 1. Tracker ID: #ADD LINK TO PIVOTAL STORY

- 2. PR Description: #Brief explanation about the Pull Request

- 3. Unit tests completed?: (Y/N)
