![](https://github.com/Online-test-system-Nomre/Nomreazma/actions/workflows/ci.yml/badge.svg)![](https://github.com/Online-test-system-Nomre/Nomreazma/actions/workflows/django.yml/badge.svg)![](https://github.com/Online-test-system-Nomre/Nomreazma/actions/workflows/codeql-analysis.yml/badge.svg)

# Nomre Azma

#### [**NomreAzma**](http://nomreazma.ir/) is a testing system.

## Description of the NomreAzma

##### This is a currently private testing project based on Python and Django, using HTML, CSS and JavaScript for server-based design.
##### Our goal in building this system is to design attractive and different tests with a smooth and interesting user interface.
##### The database used in the project is SQLite.
##### By referring to the following link, you can visit the site and send us its defects in the issues section and contact us via email :

- Website : [**NomreAzma.ir**](http://nomreazma.ir/)
- Email : <a href='mailto:info@nomreazma.ir'>info@nomreazma.ir</a>

### Thanks for using [NomreAzma](https://nomreazma.ir/)

#### Powered by **Django**
